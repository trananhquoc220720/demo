﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Claims;

namespace Client.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public HomeController(ILogger<HomeController> logger, IHttpClientFactory httpClientFactory,
                   IHttpContextAccessor httpContextAccessor,
                    IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _httpClientFactory = httpClientFactory;
        }
        //protected string GeNameFromToken()
        //{
        //    return User?.FindFirst(ClaimTypes.Name)?.Value;
        //}

        protected string GetRoleFromToken()
        {
            var a = User;
            return User?.FindFirst(ClaimTypes.Role)?.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Privacy([FromRoute] string role)
        {
            var sessions = HttpContext.Session.GetString("JWT");

            var client = _httpClientFactory.CreateClient();
            client.BaseAddress = new Uri("https://localhost:7095");
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);

            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(sessions);

            var roleRepon = jwtSecurityToken.Claims.First(claim => claim.Type == ClaimTypes.Role).Value;

            string[] roles = roleRepon.Split(';');
            HttpResponseMessage response = new();

            if (roles[0].Equals("SuperAdmin"))
            {
                response = await client.GetAsync("api/Values/SuperAdmin");

            }
            else if (roles[0] == "Admin")
            {
                response = await client.GetAsync("api/Values/GetFromUserAdmin");

            }
            else
            {
                response = await client.GetAsync("api/Values/GetFromUser");
            }
            var body = await response.Content.ReadAsStringAsync();
            var repo = JsonConvert.DeserializeObject<IEnumerable<string>>(body);
            return View(repo);
        }
    }
}