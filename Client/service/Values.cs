﻿using Demo.Api.ModelVM;
using Newtonsoft.Json;
using System.Text;

namespace Client.service
{
    public class Values
    {
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public Values(IHttpClientFactory httpClientFactory,
                   IHttpContextAccessor httpContextAccessor,
                    IConfiguration configuration)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            _httpClientFactory = httpClientFactory;
        }
        public async Task<string> GetAsync()
        {
            //var sessions = _httpContextAccessor
            //    .HttpContext
            //    .Session
            //    .GetString("JWT");
            try
            {
                var client = _httpClientFactory.CreateClient();
                client.BaseAddress = new Uri("https://localhost:7030");
                // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", sessions);
                var request = new LoginModel() { UserName = "useraccount", Password = "123456" };
                var json = JsonConvert.SerializeObject(request);
                var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await client.PostAsync("api/Token", httpContent);
                var body = await response.Content.ReadAsStringAsync();
                //if (response.IsSuccessStatusCode)
                //{
                //    string myDeserializedObjList = (string)JsonConvert.DeserializeObject(body,
                //        typeof(string));

                //    return myDeserializedObjList;
                //}
                //var a = JsonConvert.DeserializeObject<string>(body);
                return body;
            }
            catch (Exception e)
            {
                return e.Message;
            }
            //  return null;

        }
    }
}
