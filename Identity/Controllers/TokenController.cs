﻿using Identity.ModelVM;
using Identity.Service;
using Microsoft.AspNetCore.Mvc;

namespace Identity.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly ITokenService _iTokenService;

        public TokenController(ITokenService iTokenService)
        {
            _iTokenService = iTokenService;
        }
        [HttpPost]
        public IActionResult GenerateToken([FromBody] LoginModel LoginModel)
        {
            var token = _iTokenService.GenerateToken(LoginModel);
            return Ok(token);
        }

        //[HttpPost("GetRoleUserFromToken")]
        //public IActionResult GetRoleUserFromToken([FromBody] LoginModel LoginModel)
        //{
        //    var token = _iTokenService.GetRoleUserFromToken(LoginModel);
        //    return Ok(token);
        //}
    }
}
