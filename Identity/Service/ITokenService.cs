﻿using Identity.ModelVM;

namespace Identity.Service
{
    public interface ITokenService
    {
        string GenerateToken(LoginModel LoginModel);
        UserVM GetRoleUserFromToken(LoginModel LoginModel);
    }
}
