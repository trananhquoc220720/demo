﻿using Identity.Models;
using Identity.ModelVM;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Identity.Service
{
    public class TokenService : ITokenService
    {
        private DemoContext _context;

        public TokenService(DemoContext context)
        {
            _context = context;
        }
        public string GenerateToken(LoginModel LoginModel)
        {

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("0123456789ABCDEF"));
            var user = _context.Users.FirstOrDefault(x => x.UserName == LoginModel.UserName && x.Password == LoginModel.Password);
            if (user == null)
                return "sai mat khau hoac pass";

            var roles = _context.UserRoles.Where(x => x.UserId == user.Id).Select(c => c.Roles.RoleName).FirstOrDefault();
            var claims = new[]
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.Role, roles),
            };

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(3);
            var token = new JwtSecurityToken(
                "https://localhost:7030",
                "https://localhost:7030",
                claims,
                expires: expires,
                signingCredentials: creds
            );


            var repo = new JwtSecurityTokenHandler().WriteToken(token);
            return repo;
        }

        public UserVM GetRoleUserFromToken(LoginModel LoginModel)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserName == LoginModel.UserName && x.Password == LoginModel.Password);
            if (user == null)
                return null;

            var roles = _context.UserRoles.Where(x => x.UserId == user.Id).Select(c => c.Roles.RoleName).FirstOrDefault();
            return new UserVM { UserName = user.UserName, UserRoles = roles };
        }
    }
}
