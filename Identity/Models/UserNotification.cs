﻿namespace Identity.Models
{
    public partial class UserNotification
    {
        public long ReceiverId { get; set; }
        public long NotiId { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public int? Status { get; set; }
        public string? Description { get; set; }
    }
}
