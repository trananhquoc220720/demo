﻿namespace Identity.Models
{
    public class UserRole
    {
        public long UserId { get; set; }
        public int RoleId { get; set; }

        public virtual Role Roles { get; set; }
        public virtual User Users { get; set; }
    }
}
