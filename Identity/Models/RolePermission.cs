﻿namespace Identity.Models
{
    public class RolePermission
    {
        public int RoleId { get; set; }
        public int PermissionId { get; set; }

        public virtual Role Roles { get; set; }
        public virtual Permission Permissions { get; set; }
    }
}
