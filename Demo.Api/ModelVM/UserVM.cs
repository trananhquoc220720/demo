﻿namespace Demo.Api.ModelVM
{
    public class UserVM
    {
        public string UserName { get; set; }
        public string UserRoles { get; set; }
    }
}
