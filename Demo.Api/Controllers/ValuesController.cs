﻿using Demo.Api.ModelVM;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Demo.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ValuesController : ControllerBase
    {
        [Authorize(Roles = "User")]
        [HttpGet("GetFromUser")]
        public IEnumerable<string> GetFromUser()
        {
            return new string[] { "User", "User" };
        }

        [Authorize(Roles = "Admin")]
        [HttpGet("GetFromUserAdmin")]
        public IEnumerable<string> GetFromUserAdmin()
        {
            //  var a = GetCurrentUser();
            return new string[] { "admin", "admin" };
        }

        [HttpGet("GetFromSuperAdmin")]
        [Authorize(Roles = "Super Admin")]
        public IEnumerable<string> GetFromSuperAdmin()
        {
            return new string[] { "GetFromSuperAdmin", "GetFromSuperAdmin" };
        }

        private UserVM GetCurrentUser()
        {
            var identity = HttpContext.User.Identity as ClaimsIdentity;

            if (identity != null)
            {
                var userClaims = identity.Claims;

                return new UserVM
                {
                    UserName = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Name)?.Value,
                    UserRoles = userClaims.FirstOrDefault(o => o.Type == ClaimTypes.Role)?.Value
                };
            }
            return null;
        }
    }
}
